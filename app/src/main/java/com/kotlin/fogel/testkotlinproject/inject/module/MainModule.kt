package com.kotlin.fogel.testkotlinproject.inject.module

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.inject.scope.FragmentScope
import com.kotlin.fogel.testkotlinproject.presentation.interactor.main.MainInteractor
import com.kotlin.fogel.testkotlinproject.presentation.interactor.main.MainInteractorImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.common.pokemonadapter.PokemonRecyclerViewAdapter
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @FragmentScope
    fun provideMainInteractor(pokemonService: PokemonService): MainInteractor {
        return MainInteractorImpl(pokemonService)
    }

    @Provides
    @FragmentScope
    fun providePokemonAdapter(): PokemonRecyclerViewAdapter {
        return PokemonRecyclerViewAdapter()
    }

}