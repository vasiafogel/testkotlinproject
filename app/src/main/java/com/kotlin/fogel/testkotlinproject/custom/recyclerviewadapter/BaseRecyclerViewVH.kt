package com.kotlin.fogel.testkotlinproject.custom.recyclerviewadapter

import android.support.v7.widget.RecyclerView
import android.view.View


abstract class BaseRecyclerViewVH<T>(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    abstract fun bindData(data: T?)

    abstract fun setListeners(listener: ((T) -> Unit)?, item: T?)

}