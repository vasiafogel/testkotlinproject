package com.kotlin.fogel.testkotlinproject.inject.scope

import javax.inject.Scope

@Scope
annotation class ActivityScope