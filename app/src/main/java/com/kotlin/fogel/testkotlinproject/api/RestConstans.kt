package com.kotlin.fogel.testkotlinproject.api

class RestConstans {
    companion object {
        val BASE_URL = "https://pokeapi.co/api/v2/"
        // : "http://45.77.55.49:3031/"

        val HEADER_AUTH = "authorization"
        val HEADER_ACCEPT = "Accept"
        val HEADER_CONTENT_TYPE = "Content-Type"
        val HEADER_VALUE_HTML = "text/html"
        val HEADER_VALUE_JSON = "application/json"

        val TIMEOUT: Long = 30 //seconds
        val TIMEOUT_READ: Long = 30 //seconds
        val TIMEOUT_WRITE: Long = 60 //seconds
    }
}