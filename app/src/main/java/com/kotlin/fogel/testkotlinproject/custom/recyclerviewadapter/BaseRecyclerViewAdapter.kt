package com.kotlin.fogel.testkotlinproject.custom.recyclerviewadapter

interface BaseRecyclerViewAdapter<T> {

    fun addAllAndClear(list: Collection<T?>)

    fun addAll(list: Collection<T?>)

    fun addAllToStart(list: Collection<T?>)

    fun add(item: T?)

    fun addToStart(item: T?)

    fun clear()

    fun remove(position: Int)

    fun removeSame()

    fun removeLast()

    fun removeFirst()

    fun getLastItem(): T?

    fun updateItem(position: Int)

    fun changeItem(item: T?, position: Int)

    fun updateList()

}