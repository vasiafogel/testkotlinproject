package com.kotlin.fogel.testkotlinproject.presentation.view.tutorial

import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseView


interface TutorialView : BaseView {

    fun openActivity()

}