package com.kotlin.fogel.testkotlinproject.inject.component

import com.kotlin.fogel.testkotlinproject.inject.module.*
import com.kotlin.fogel.testkotlinproject.presentation.view.splash.SplashActivity
import com.kotlin.fogel.testkotlinproject.presentation.view.tutorial.TutorialActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (RestModule::class)])
interface ApplicationComponent {

    fun plus(module: MainModule): MainComponent
    fun plus(module: DetailsModule): DetailsComponent
    fun plus(module: TypesModule): TypesComponent
    fun plus(module: TypePageModule): TypePageComponent
    fun plus(module: TutorialModule): TutorialComponent
    fun inject(activity: SplashActivity)

}