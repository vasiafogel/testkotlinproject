package com.kotlin.fogel.testkotlinproject.presentation.view.types

import com.kotlin.fogel.testkotlinproject.model.Type
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseView


interface TypesView : BaseView {

    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun showPokemonTypes(pokemonTypes: List<Type>)
    fun showDoneMessage()
    fun showErrorMessage()

}