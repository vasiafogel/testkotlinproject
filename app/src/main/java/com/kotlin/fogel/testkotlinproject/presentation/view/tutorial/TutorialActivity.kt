package com.kotlin.fogel.testkotlinproject.presentation.view.tutorial

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import com.kotlin.fogel.testkotlinproject.R
import com.kotlin.fogel.testkotlinproject.extension.app
import com.kotlin.fogel.testkotlinproject.presentation.presenter.loader.PresenterFactory
import com.kotlin.fogel.testkotlinproject.presentation.presenter.tutorial.TutorialPresenter
import com.kotlin.fogel.testkotlinproject.presentation.presenter.tutorial.TutorialPresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseRetainActivity
import com.kotlin.fogel.testkotlinproject.presentation.view.main.MainActivity
import com.kotlin.fogel.testkotlinproject.custom.viewpager.adapter.BaseViewPagerAdapter
import com.kotlin.fogel.testkotlinproject.inject.module.TutorialModule
import com.kotlin.fogel.testkotlinproject.util.Constants
import kotlinx.android.synthetic.main.activity_tutorial.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class TutorialActivity : BaseRetainActivity<TutorialPresenter, TutorialView>(), TutorialView {

    @Inject
    lateinit var sharedPreference: SharedPreferences

    @Inject
    lateinit var mPagerAdapter: BaseViewPagerAdapter

    override fun injectDependencies() {
        app.applicationComponent
                .plus(TutorialModule())
                .injectTo(this)
    }

    override fun presenterFactory(): PresenterFactory<TutorialPresenter> {
        return object : PresenterFactory<TutorialPresenter>{
            override fun create(): TutorialPresenter {
                return TutorialPresenterImpl(sharedPreference)
            }
        }
    }

    override fun openActivity() {
        startActivity(intentFor<MainActivity>().clearTask())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)
        initUI()
    }

    private fun initUI(){
        RxView.clicks(btnStartUsing_AT)
                .throttleFirst(Constants.CLICK_DELAY.toLong(), TimeUnit.MILLISECONDS)
                .subscribe{presenter?.skip()}
        mPagerAdapter.serArrayOfViews(arrayOf(
                R.layout.view_tutorial_first_page,
                R.layout.view_tutorial_second,
                R.layout.view_tutorial_third))
        vpTutorialContainer_AT.adapter = mPagerAdapter
        indicator_AT.setViewPager(vpTutorialContainer_AT)
        vpTutorialContainer_AT.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                if (position == (mPagerAdapter.count - 1)) {
                    btnStartUsing_AT.visibility = View.VISIBLE
                } else {
                    btnStartUsing_AT.visibility = View.INVISIBLE
                }
            }
        })
    }

}