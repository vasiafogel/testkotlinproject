package com.kotlin.fogel.testkotlinproject.model

import android.os.Parcel
import android.os.Parcelable

data class APIResourceList(val count: Int,
                           val next: String,
                           val previous: Boolean,
                           val results: List<NamedAPIResource>) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.createTypedArrayList(NamedAPIResource))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(count)
        parcel.writeString(next)
        parcel.writeByte(if (previous) 1 else 0)
        parcel.writeTypedList(results)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<APIResourceList> {
        override fun createFromParcel(parcel: Parcel): APIResourceList {
            return APIResourceList(parcel)
        }

        override fun newArray(size: Int): Array<APIResourceList?> {
            return arrayOfNulls(size)
        }
    }
}
