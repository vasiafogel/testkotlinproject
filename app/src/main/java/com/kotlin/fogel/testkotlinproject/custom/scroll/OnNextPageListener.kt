package com.kotlin.fogel.testkotlinproject.custom.scroll


interface OnNextPageListener {
    fun onLoadMore(): Boolean
}