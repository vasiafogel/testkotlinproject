package com.kotlin.fogel.testkotlinproject.presentation.interactor.main

import android.util.Log
import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class MainInteractorImpl(private val pokemonService: PokemonService) : MainInteractor {

    private var disposable: Disposable? = null

    override var networkRequestInProgress = false

    override fun getPokemonList(listener: MainInteractor.OnGetPokemonListListener, limit: Int, offset: Int) {
        networkRequestInProgress = true
        Log.d("Error", "Hello it is me")
        disposable = pokemonService.getPokemonList(limit, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    it.results.map {
                        val pokemonId = it.url.split("/")[6].toInt()
                        Pokemon(pokemonId, it.name)
                    }
                }
                .subscribe({ pokemonList ->
                    networkRequestInProgress = false
                    listener.onSuccess(pokemonList)
                }, { error ->
                    error.printStackTrace()
                    networkRequestInProgress = false
                    listener.onFailure()
                })
    }

    override fun cancelNetworkRequest() {
        disposable?.dispose()
    }

}