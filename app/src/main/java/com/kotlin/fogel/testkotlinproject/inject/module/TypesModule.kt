package com.kotlin.fogel.testkotlinproject.inject.module

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.inject.scope.ActivityScope
import com.kotlin.fogel.testkotlinproject.presentation.interactor.types.TypesInteractor
import com.kotlin.fogel.testkotlinproject.presentation.interactor.types.TypesInteractorImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.types.TypesActivity
import com.kotlin.fogel.testkotlinproject.presentation.view.types.adapter.TypesFragmentPagerAdapter
import dagger.Module
import dagger.Provides

@Module
class TypesModule(private val activity: TypesActivity) {

    @Provides
    @ActivityScope
    fun provideTypesInteractor(pokemonService: PokemonService): TypesInteractor {
        return TypesInteractorImpl(pokemonService)
    }

    @Provides
    @ActivityScope
    fun provideTypesFragmentPagerAdapter(): TypesFragmentPagerAdapter {
        return TypesFragmentPagerAdapter(activity.supportFragmentManager)
    }

}