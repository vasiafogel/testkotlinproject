package com.kotlin.fogel.testkotlinproject.util

import android.support.annotation.StringRes
import com.kotlin.fogel.testkotlinproject.R

class Constants {
    companion object {
        val CLICK_DELAY = 600
        val CLICK_DELAY_SMALL = 50


        /*MULTIPART REQUEST CODES*/
        val MEDIA_TYPE_IMG = "image/jpeg"
        val MEDIA_TYPE_TEXT = "text/plain"
        val UPDATE_AVATAR_KEY = "avatar"
        val UPDATE_FIRST_NAME_KEY = "firstName"
        val UPDATE_LAST_NAME_KEY = "lastName"
        val UPDATE_COUNTRY_KEY = "counry"
        val MIME_TYPE = "text/html"
        val DEFAULT_UNCODING = "UTF-8"

        enum class MessageType(@param:StringRes @field:StringRes
                               val messageRes: Int, val isDangerous: Boolean) {
            CONNECTION_PROBLEMS(R.string.err_msg_connection_problem, true),
            USER_NOT_REGISTERED(R.string.err_msg_user_not_registered, true),
            BAD_CREDENTIALS(R.string.err_msg_bad_credentials, true),
            INVALID_EMAIL_OR_SOME_FIELDS_EMPTY(R.string.err_msg_invalid_email_or_empty_fields, true),
            EMPTY_FIELDS_OR_NOT_MATCHES_PASSWORDS(R.string.err_msg_invalid_email_or_empty_fields, true),
            UNKNOWN(R.string.err_msg_something_goes_wrong, true),
            PLAY_SERVICES_UNAVAILABLE(R.string.err_msg_play_services_unavailable, true),
            ERROR_WHILE_SELECT_ADDRESS(R.string.err_msg_select_address_fail, true),
            REQUEST_SENT(R.string.err_msg_request_is_sent, false),
            SELECT_START_POINT(R.string.err_msg_select_start_point, true),
            SELECT_DESTINATION(R.string.err_msg_select_destination, true),
            SELECT_ROUTE(R.string.err_msg_select_route, true),
            SPECIFY_DEPARTURE_TIME(R.string.err_msg_specify_departure_time, true),
            SPECIFY_RIDE_FLEXIBILITY(R.string.err_msg_specify_ride_flexibility, true),
            SELECT_RATING(R.string.err_msg_set_rating, true),
            TOKEN_EXPIRED(R.string.err_msg_token_expired, true),
            SELECT_FUTURE_DATE(R.string.err_msg_select_date_in_future, true)
        }
    }
}
