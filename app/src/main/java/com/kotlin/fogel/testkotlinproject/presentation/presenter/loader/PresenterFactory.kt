package com.kotlin.fogel.testkotlinproject.presentation.presenter.loader

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter


interface PresenterFactory<out P: BasePresenter<*>> {

    fun create(): P

}