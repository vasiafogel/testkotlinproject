package com.kotlin.fogel.testkotlinproject.presentation.presenter.main

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter
import com.kotlin.fogel.testkotlinproject.presentation.view.main.MainView


interface MainPresenter : BasePresenter<MainView> {

    fun getPokemonList(offset: Int)
    fun loadNext()

}