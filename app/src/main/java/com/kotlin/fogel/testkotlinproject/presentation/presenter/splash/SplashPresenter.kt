package com.kotlin.fogel.testkotlinproject.presentation.presenter.splash

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter
import com.kotlin.fogel.testkotlinproject.presentation.view.splash.SplashView


interface SplashPresenter : BasePresenter<SplashView>