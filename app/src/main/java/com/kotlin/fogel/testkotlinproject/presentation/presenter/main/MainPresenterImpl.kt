package com.kotlin.fogel.testkotlinproject.presentation.presenter.main

import com.kotlin.fogel.testkotlinproject.presentation.interactor.main.MainInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.main.MainView


class MainPresenterImpl(private val interactor: MainInteractor) :
        BasePresenterImpl<MainView>(), MainPresenter, MainInteractor.OnGetPokemonListListener {


    override fun loadNext() {
        getPokemonList(pokemonList.size)
    }

    private var pokemonList = ArrayList<Pokemon?>()

    private val limit: Int = 100

    override fun onStart(firstStart: Boolean) {
        if (firstStart) {
            getPokemonList(0)
        } else {
            if (interactor.networkRequestInProgress) {
                view?.showLoadingIndicator()
            }
        }
    }

    override fun onStop() {
        view?.hideLoadingIndicator()
    }

    override fun onPresenterDestroyed() {
        interactor.cancelNetworkRequest()
    }

    // MainPresenter

    override fun getPokemonList(offset: Int) {
        if (!interactor.networkRequestInProgress) {
            if (offset == 0) {
                pokemonList.clear()
                view?.showLoadingIndicator()
            }
            interactor.getPokemonList(this, limit, pokemonList.size)
        }
    }

    // OnGetPokemonListListener

    override fun onSuccess(pokemonList: List<Pokemon>) {
        this.pokemonList.addAll(pokemonList)
        view?.showPokemonList(pokemonList)
        view?.hideLoadingIndicator()
        view?.showDoneMessage()
    }

    override fun onFailure() {
        view?.hideLoadingIndicator()
        view?.showErrorMessage()
    }
}