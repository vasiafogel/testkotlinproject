package com.kotlin.fogel.testkotlinproject.inject.component

import com.kotlin.fogel.testkotlinproject.inject.module.TutorialModule
import com.kotlin.fogel.testkotlinproject.inject.scope.ActivityScope
import com.kotlin.fogel.testkotlinproject.presentation.view.tutorial.TutorialActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [(TutorialModule::class)])
interface TutorialComponent {

    fun injectTo(activity: TutorialActivity)

}