package com.kotlin.fogel.testkotlinproject.api.exceptions

class TimeoutException(msg: String = "Server doesn't respond") : RuntimeException(msg)