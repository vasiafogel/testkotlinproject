package com.kotlin.fogel.testkotlinproject.presentation.view.common.pokemonadapter

import android.view.View
import com.kotlin.fogel.testkotlinproject.R
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.custom.recyclerviewadapter.BaseRecyclerViewAdapterImpl
import com.kotlin.fogel.testkotlinproject.custom.recyclerviewadapter.BaseRecyclerViewVH


class PokemonRecyclerViewAdapter : BaseRecyclerViewAdapterImpl<Pokemon>() {

    private val item: Int = 0
    private val loading: Int = 1

    override fun getLayout(viewType: Int): Int {
        return when (viewType) {
            item -> R.layout.item_pokemon
            loading -> R.layout.item_loading
            else -> R.layout.item_loading
        }
    }

    override fun getViewHolder(view: View, viewType: Int): BaseRecyclerViewVH<Pokemon>? {
        return when (viewType) {
            item -> ItemViewHolder(view)
            loading -> LoaderViewHolder(view)
            else -> LoaderViewHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position] == null) loading else item
    }


}