package com.kotlin.fogel.testkotlinproject.custom.viewpager.adapter

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotlin.fogel.testkotlinproject.R

class BaseViewPagerAdapter : PagerAdapter() {

    private lateinit var array: Array<Int>

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(container.context)
        val view: View? = inflater.inflate(array[position], container, false)
        container.addView(view)
        return view!!
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = array.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun serArrayOfViews(array: Array<Int>) {
        this.array = array
    }

}