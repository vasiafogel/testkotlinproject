package com.kotlin.fogel.testkotlinproject.inject.module

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.inject.scope.FragmentScope
import com.kotlin.fogel.testkotlinproject.presentation.interactor.types.typePage.TypePageInteractor
import com.kotlin.fogel.testkotlinproject.presentation.interactor.types.typePage.TypePageInteractorImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.common.pokemonadapter.PokemonRecyclerViewAdapter
import dagger.Module
import dagger.Provides

@Module
class TypePageModule {

    @Provides
    @FragmentScope
    fun provideTypePageInteractor(pokemonService: PokemonService): TypePageInteractor {
        return TypePageInteractorImpl(pokemonService)
    }

    @Provides
    @FragmentScope
    fun providePokemonAdapter(): PokemonRecyclerViewAdapter {
        return PokemonRecyclerViewAdapter()
    }

}