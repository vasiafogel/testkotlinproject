package com.kotlin.fogel.testkotlinproject.presentation.interactor.details

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class DetailsInteractorImpl(private val pokemonService: PokemonService) : DetailsInteractor {

    private var disposable: Disposable? = null

    override var networkRequestInProgress = false

    override fun getPokemonDetails(id: Int, listener: DetailsInteractor.OnGetPokemonDetailsListener) {
        networkRequestInProgress = true
        disposable = pokemonService.getPokemonDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pokemon ->
                    networkRequestInProgress = false
                    listener.onSuccess(pokemon)
                }, { error ->
                    error.printStackTrace()
                    networkRequestInProgress = false
                    listener.onFailure()
                })
    }

    override fun cancelNetworkRequest() {
        disposable?.dispose()
    }

}