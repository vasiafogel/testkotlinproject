package com.kotlin.fogel.testkotlinproject.api.interceptors

import android.preference.PreferenceManager
import android.text.TextUtils
import com.kotlin.fogel.testkotlinproject.App
import com.kotlin.fogel.testkotlinproject.api.RestConstans
import okhttp3.Interceptor
import okhttp3.Response

class SaveTokenInterceptor(private val app: App) : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val originalResponse: Response = chain!!.proceed(chain.request())
        val auth: String? = originalResponse.header(RestConstans.HEADER_AUTH)
        if (TextUtils.isEmpty(auth)){
            PreferenceManager
                    .getDefaultSharedPreferences(app)
                    .edit()
                    .putString("getAccessToken", auth)
                    .apply()
        }
        return originalResponse
    }
}