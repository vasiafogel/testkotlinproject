package com.kotlin.fogel.testkotlinproject.inject.module

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.inject.scope.ActivityScope
import com.kotlin.fogel.testkotlinproject.presentation.interactor.details.DetailsInteractor
import com.kotlin.fogel.testkotlinproject.presentation.interactor.details.DetailsInteractorImpl
import dagger.Module
import dagger.Provides

@Module
class DetailsModule {

    @Provides
    @ActivityScope
    fun provideDetailsInteractor(pokemonService: PokemonService): DetailsInteractor {
        return DetailsInteractorImpl(pokemonService)
    }

}