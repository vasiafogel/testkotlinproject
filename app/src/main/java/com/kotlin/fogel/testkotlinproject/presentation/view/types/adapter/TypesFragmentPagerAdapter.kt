package com.kotlin.fogel.testkotlinproject.presentation.view.types.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.kotlin.fogel.testkotlinproject.presentation.view.types.typePage.TypePageFragment


class TypesFragmentPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var fragments = listOf<TypePageFragment>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getPageTitle(position: Int): String? =
            fragments[position].arguments!!.getString(TypePageFragment.ARG_TYPE_NAME)

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size
}