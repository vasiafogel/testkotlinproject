package com.kotlin.fogel.testkotlinproject.inject.module

import android.content.Context.LOCATION_SERVICE
import android.content.SharedPreferences
import android.location.LocationManager
import android.preference.PreferenceManager
import com.kotlin.fogel.testkotlinproject.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    @Provides
    @Singleton
    fun provideLocationManager(): LocationManager {
        return app.getSystemService(LOCATION_SERVICE) as LocationManager
    }

}