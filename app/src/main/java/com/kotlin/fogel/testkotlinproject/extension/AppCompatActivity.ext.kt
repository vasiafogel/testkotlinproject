package com.kotlin.fogel.testkotlinproject.extension


import android.support.v7.app.AppCompatActivity
import com.kotlin.fogel.testkotlinproject.App
import kotlinx.android.synthetic.main.template_toolbar.*

val AppCompatActivity.app: App
    get() = application as App

fun AppCompatActivity.initToolbar(title: String? = null, hasParent: Boolean = false) {
    setSupportActionBar(toolbar)

    // set title (if needed)
    title?.let {
        supportActionBar?.title = it
    }

    // set back arrow (if needed)
    if (hasParent) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}