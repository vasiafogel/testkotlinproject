package com.kotlin.fogel.testkotlinproject.model

import android.os.Parcel
import android.os.Parcelable

data class TypePokemon(
        val slot: Int,
        val pokemon: NamedAPIResource) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readParcelable(NamedAPIResource::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(slot)
        parcel.writeParcelable(pokemon, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TypePokemon> {
        override fun createFromParcel(parcel: Parcel): TypePokemon {
            return TypePokemon(parcel)
        }

        override fun newArray(size: Int): Array<TypePokemon?> {
            return arrayOfNulls(size)
        }
    }
}