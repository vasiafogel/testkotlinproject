package com.kotlin.fogel.testkotlinproject.presentation.interactor.main

import com.kotlin.fogel.testkotlinproject.presentation.interactor.base.BaseInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon


interface MainInteractor : BaseInteractor {

    interface OnGetPokemonListListener {
        fun onSuccess(pokemonList: List<Pokemon>)
        fun onFailure()
    }

    var networkRequestInProgress: Boolean

    fun getPokemonList(
            listener: OnGetPokemonListListener,
            limit: Int,
            offset: Int)
    fun cancelNetworkRequest()

}