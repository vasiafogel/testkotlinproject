package com.kotlin.fogel.testkotlinproject.inject.module

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.kotlin.fogel.testkotlinproject.App
import com.kotlin.fogel.testkotlinproject.BuildConfig
import com.kotlin.fogel.testkotlinproject.api.RestConstans
import com.kotlin.fogel.testkotlinproject.api.interceptors.ApiInterceptor
import com.kotlin.fogel.testkotlinproject.api.interceptors.SaveTokenInterceptor
import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class RestModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    @Provides
    @Singleton
    fun provideCache(app: App): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(app.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(app: App, cache: Cache): OkHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(RestConstans.TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(RestConstans.TIMEOUT_READ, TimeUnit.SECONDS)
                .writeTimeout(RestConstans.TIMEOUT_WRITE, TimeUnit.SECONDS)
                .addNetworkInterceptor(StethoInterceptor())
                .addInterceptor(ApiInterceptor(app))
                .addInterceptor(SaveTokenInterceptor(app))
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                })
                .cache(cache)
                .build()

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(RestConstans.BASE_URL)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun provideAuthService(retrofit: Retrofit): PokemonService = retrofit.create(PokemonService::class.java)
}