package com.kotlin.fogel.testkotlinproject.presentation.interactor.types

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.model.Type
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class TypesInteractorImpl(private val pokemonService: PokemonService) : TypesInteractor {

    private var disposable: Disposable? = null

    override var networkRequestInProgress = false

    override fun getPokemonTypes(listener: TypesInteractor.OnGetPokemonTypesListener) {
        networkRequestInProgress = true
        disposable = pokemonService.getPokemonTypes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    it.results.map {
                        val typeId = it.url.split("/")[6].toInt()
                        Type(typeId, it.name)
                    }
                }
                .subscribe({ pokemonTypes ->
                    networkRequestInProgress = false
                    listener.onSuccess(pokemonTypes)
                }, { error ->
                    error.printStackTrace()
                    networkRequestInProgress = false
                    listener.onFailure()
                })
    }

    override fun cancelNetworkRequest() {
        disposable?.dispose()
    }

}