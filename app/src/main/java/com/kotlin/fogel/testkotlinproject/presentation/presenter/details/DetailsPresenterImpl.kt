package com.kotlin.fogel.testkotlinproject.presentation.presenter.details

import com.kotlin.fogel.testkotlinproject.presentation.interactor.details.DetailsInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.details.DetailsView


class DetailsPresenterImpl(private val interactor: DetailsInteractor) :
        BasePresenterImpl<DetailsView>(), DetailsPresenter, DetailsInteractor.OnGetPokemonDetailsListener {

    private var pokemon: Pokemon? = null

    override fun onStart(firstStart: Boolean) {
        if (firstStart) {
            view?.getPokemonId()?.let {
                getPokemonDetails(it)
            }
        } else {
            if (interactor.networkRequestInProgress) {
                view?.showLoadingIndicator()
            }
            pokemon?.let {
                view?.showPokemonDetails(it)
            }
        }
    }

    override fun onStop() {
        view?.hideLoadingIndicator()
    }

    override fun onPresenterDestroyed() {
        interactor.cancelNetworkRequest()
    }

    // DetailsPresenter

    override fun getPokemonDetails(id: Int) {
        view?.showLoadingIndicator()
        interactor.getPokemonDetails(id, this)
    }

    // OnGetPokemonDetailsListener

    override fun onSuccess(pokemon: Pokemon) {
        this.pokemon = pokemon

        view?.showPokemonDetails(pokemon)
        view?.hideLoadingIndicator()
        view?.showDoneMessage()
    }

    override fun onFailure() {
        view?.hideLoadingIndicator()
        view?.showErrorMessage()
    }
}