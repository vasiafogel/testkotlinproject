package com.kotlin.fogel.testkotlinproject.presentation.presenter.types.typePage

import com.kotlin.fogel.testkotlinproject.presentation.interactor.types.typePage.TypePageInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.types.typePage.TypePageView


class TypePagePresenterImpl(private val interactor: TypePageInteractor) :
        BasePresenterImpl<TypePageView>(), TypePagePresenter, TypePageInteractor.OnGetAllPokemonOfTypeListener {

    private var pokemonList = listOf<Pokemon>()

    override fun onStart(firstStart: Boolean) {
        if (firstStart) {
            view?.getPokemonTypeId()?.let {
                getAllPokemonOfType(it)
            }
        } else {
            if (interactor.networkRequestInProgress) {
                view?.showLoadingIndicator()
            }
            view?.showAllPokemonOfType(pokemonList)
        }
    }

    override fun onStop() {
        view?.hideLoadingIndicator()
    }

    override fun onPresenterDestroyed() {
        interactor.cancelNetworkRequest()
    }

    // TypePagePresenter

    override fun getAllPokemonOfType(id: Int) {
        view?.showLoadingIndicator()
        interactor.getAllPokemonOfType(id, this)
    }

    // OnGetAllPokemonOfTypeListener

    override fun onSuccess(pokemonList: List<Pokemon>) {
        this.pokemonList = pokemonList

        view?.showAllPokemonOfType(pokemonList)
        view?.hideLoadingIndicator()
        view?.showDoneMessage()
    }

    override fun onFailure() {
        view?.hideLoadingIndicator()
        view?.showErrorMessage()
    }
}