package com.kotlin.fogel.testkotlinproject

import android.app.Application
import com.facebook.stetho.Stetho
import com.kotlin.fogel.testkotlinproject.inject.component.ApplicationComponent
import com.kotlin.fogel.testkotlinproject.inject.component.DaggerApplicationComponent
import com.kotlin.fogel.testkotlinproject.inject.module.AppModule
import com.kotlin.fogel.testkotlinproject.inject.module.RestModule
import org.jetbrains.anko.connectivityManager

open class App: Application() {

    open val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
                .appModule(AppModule(this))
                .restModule(RestModule())
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

    fun hasInternetConnection(): Boolean =
            connectivityManager.activeNetworkInfo != null
                    && connectivityManager.activeNetworkInfo.isConnectedOrConnecting


}