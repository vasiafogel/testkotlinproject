package com.kotlin.fogel.testkotlinproject.api.interceptors

import android.preference.PreferenceManager
import com.kotlin.fogel.testkotlinproject.App
import com.kotlin.fogel.testkotlinproject.api.RestConstans
import com.kotlin.fogel.testkotlinproject.api.exceptions.ConnectionLostException
import com.kotlin.fogel.testkotlinproject.api.exceptions.TimeoutException
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.net.SocketTimeoutException

class ApiInterceptor(private val app: App) : Interceptor{

    override fun intercept(chain: Interceptor.Chain?): Response {
        try {
            if (!app.hasInternetConnection()) {
                throw ConnectionLostException()
            } else {
                val requestBuilder: Request.Builder  = chain!!.request().newBuilder()
                        .header(RestConstans.HEADER_CONTENT_TYPE, RestConstans.HEADER_VALUE_HTML)
                requestBuilder.header(RestConstans.HEADER_AUTH,
                        PreferenceManager.getDefaultSharedPreferences(app).getString("getAccessToken", ""))
                return chain.proceed(requestBuilder.build())
            }
        } catch (e: SocketTimeoutException) {
            throw TimeoutException()
        }
    }

}