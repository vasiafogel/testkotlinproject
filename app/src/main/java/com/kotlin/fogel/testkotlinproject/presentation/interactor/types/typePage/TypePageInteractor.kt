package com.kotlin.fogel.testkotlinproject.presentation.interactor.types.typePage

import com.kotlin.fogel.testkotlinproject.presentation.interactor.base.BaseInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon


interface TypePageInteractor : BaseInteractor {

    interface OnGetAllPokemonOfTypeListener {
        fun onSuccess(pokemonList: List<Pokemon>)
        fun onFailure()
    }

    var networkRequestInProgress: Boolean

    fun getAllPokemonOfType(id: Int, listener: OnGetAllPokemonOfTypeListener)
    fun cancelNetworkRequest()

}