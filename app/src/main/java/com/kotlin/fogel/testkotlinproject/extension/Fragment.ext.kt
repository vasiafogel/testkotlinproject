package com.kotlin.fogel.testkotlinproject.extension

import android.support.v4.app.Fragment
import com.kotlin.fogel.testkotlinproject.App

val Fragment.app: App
    get() = activity!!.application as App