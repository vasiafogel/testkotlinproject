package com.kotlin.fogel.testkotlinproject.api.service

import com.kotlin.fogel.testkotlinproject.model.APIResourceList
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.model.Type
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface PokemonService {

    @GET("pokemon")
    fun getPokemonList(@Query("limit") limit: Int, @Query("offset") offset: Int): Single<APIResourceList>

    @GET("pokemon/{id}")
    fun getPokemonDetails(@Path("id") id: Int): Single<Pokemon>

    @GET("type")
    fun getPokemonTypes(): Single<APIResourceList>

    @GET("type/{id}")
    fun getPokemonType(@Path("id") id: Int): Single<Type>

}