package com.kotlin.fogel.testkotlinproject.presentation.view.main

import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import com.kotlin.fogel.testkotlinproject.R
import com.kotlin.fogel.testkotlinproject.extension.initToolbar
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseActivity
import com.kotlin.fogel.testkotlinproject.presentation.view.types.TypesActivity
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity


class MainActivity : BaseActivity() {

    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initToolbar()

        var mainFragment = supportFragmentManager.findFragmentByTag(MainFragment.TAG)
        if (mainFragment == null) {
            mainFragment = MainFragment()
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.fragment_container, mainFragment, MainFragment.TAG)
                    .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.types -> {
                startActivity<TypesActivity>()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (doubleBackToExitPressedOnce) {
            finish()
        } else {
            doubleBackToExitPressedOnce = true
            longToast("Please click BACK again to exit")
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }


}