package com.kotlin.fogel.testkotlinproject.inject.component

import com.kotlin.fogel.testkotlinproject.inject.module.TypesModule
import com.kotlin.fogel.testkotlinproject.inject.scope.ActivityScope
import com.kotlin.fogel.testkotlinproject.presentation.view.types.TypesActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [(TypesModule::class)])
interface TypesComponent {


    fun injectTo(activity: TypesActivity)

}