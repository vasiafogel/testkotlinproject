package com.kotlin.fogel.testkotlinproject.inject.component

import com.kotlin.fogel.testkotlinproject.inject.module.TypePageModule
import com.kotlin.fogel.testkotlinproject.inject.scope.FragmentScope
import com.kotlin.fogel.testkotlinproject.presentation.view.types.typePage.TypePageFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [(TypePageModule::class)])
interface TypePageComponent {

    fun injectTo(fragment: TypePageFragment)

}