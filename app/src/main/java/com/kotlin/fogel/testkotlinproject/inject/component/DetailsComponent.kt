package com.kotlin.fogel.testkotlinproject.inject.component

import com.kotlin.fogel.testkotlinproject.inject.module.DetailsModule
import com.kotlin.fogel.testkotlinproject.inject.scope.ActivityScope
import com.kotlin.fogel.testkotlinproject.presentation.view.details.DetailsActivity
import dagger.Subcomponent


@ActivityScope
@Subcomponent(modules = [(DetailsModule::class)])
interface DetailsComponent {

    fun injectTo(activity: DetailsActivity)

}