package com.kotlin.fogel.testkotlinproject.presentation.presenter.types.typePage

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter
import com.kotlin.fogel.testkotlinproject.presentation.view.types.typePage.TypePageView


interface TypePagePresenter : BasePresenter<TypePageView> {

    fun getAllPokemonOfType(id: Int)

}