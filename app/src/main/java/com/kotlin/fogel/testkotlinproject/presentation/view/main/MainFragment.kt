package com.kotlin.fogel.testkotlinproject.presentation.view.main

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotlin.fogel.testkotlinproject.R
import com.kotlin.fogel.testkotlinproject.custom.scroll.EndlessScrollListener
import com.kotlin.fogel.testkotlinproject.custom.scroll.OnNextPageListener
import com.kotlin.fogel.testkotlinproject.extension.app
import com.kotlin.fogel.testkotlinproject.inject.module.MainModule
import com.kotlin.fogel.testkotlinproject.presentation.interactor.main.MainInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.presenter.loader.PresenterFactory
import com.kotlin.fogel.testkotlinproject.presentation.presenter.main.MainPresenter
import com.kotlin.fogel.testkotlinproject.presentation.presenter.main.MainPresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseRetainFragment
import com.kotlin.fogel.testkotlinproject.presentation.view.common.pokemonadapter.PokemonRecyclerViewAdapter
import com.kotlin.fogel.testkotlinproject.presentation.view.details.DetailsActivity
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.startActivity
import javax.inject.Inject


class MainFragment : BaseRetainFragment<MainPresenter, MainView>(), MainView {

    companion object {
        val TAG = "MainFragment"
    }

    @Inject
    lateinit var interactor: MainInteractor

    lateinit var mScrollListener: EndlessScrollListener

    @Inject
    lateinit var pokemonRecyclerViewAdapter: PokemonRecyclerViewAdapter

    override fun injectDependencies() {
        app.applicationComponent
                .plus(MainModule())
                .injectTo(this)
    }

    override fun presenterFactory(): PresenterFactory<MainPresenter> {
        return object : PresenterFactory<MainPresenter> {
            override fun create(): MainPresenter {
                return MainPresenterImpl(interactor)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        initSwipeRefreshLayout(view.find(R.id.swipe_refresh_layout))
        initRecyclerView(view.find(R.id.recycler_view))

        return view
    }

    private fun initSwipeRefreshLayout(swipeRefreshLayout: SwipeRefreshLayout) {
        swipeRefreshLayout.apply {
            setColorSchemeResources(R.color.colorAccent)
            setOnRefreshListener {
                pokemonRecyclerViewAdapter.clear()
                mScrollListener.reset()
                presenter?.getPokemonList(0)
            }
        }
    }

    private fun initRecyclerView(recyclerView: RecyclerView) {
        pokemonRecyclerViewAdapter.setOnCardClickListener({ (id, name) ->
            startActivity<DetailsActivity>(
                    DetailsActivity.EXTRA_POKEMON_ID to id,
                    DetailsActivity.EXTRA_POKEMON_NAME to name
            )
        })
        val llm = LinearLayoutManager(activity)
        mScrollListener = EndlessScrollListener(llm, object : OnNextPageListener {
            override fun onLoadMore(): Boolean {
                if(!mScrollListener.loading) {
                    if (pokemonRecyclerViewAdapter.itemCount != 0
                            && pokemonRecyclerViewAdapter.getLastItem() != null) {
                        presenter?.loadNext()
                        pokemonRecyclerViewAdapter.add(null)
                    }
                }
                return true
            }
        })
        recyclerView.apply {
            adapter = pokemonRecyclerViewAdapter
            layoutManager = llm
            addOnScrollListener(mScrollListener)
        }
    }

    // MainView

    override fun showLoadingIndicator() {
        swipe_refresh_layout.isRefreshing = true
    }

    override fun hideLoadingIndicator() {
        swipe_refresh_layout.isRefreshing = false
    }

    override fun showPokemonList(pokemonList: List<Pokemon?>) {
        if (pokemonRecyclerViewAdapter.getLastItem() == null) {
            pokemonRecyclerViewAdapter.removeLast()
        }
        pokemonRecyclerViewAdapter.addAll(pokemonList)
    }

    override fun showDoneMessage() {
        Snackbar.make(activity!!.coordinator_layout, R.string.done, Snackbar.LENGTH_SHORT)
                .show()
    }

    override fun showErrorMessage() {
        if (pokemonRecyclerViewAdapter.getLastItem() == null) {
            pokemonRecyclerViewAdapter.removeLast()
        }
        Snackbar.make(activity!!.coordinator_layout, R.string.error, Snackbar.LENGTH_SHORT)
                .setAction(R.string.retry) {
                    pokemonRecyclerViewAdapter.clear()
                    mScrollListener.reset()
                    presenter?.getPokemonList(0)
                }
                .show()
    }

}