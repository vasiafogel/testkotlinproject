package com.kotlin.fogel.testkotlinproject.presentation.presenter.details

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter
import com.kotlin.fogel.testkotlinproject.presentation.view.details.DetailsView


interface DetailsPresenter : BasePresenter<DetailsView> {

    fun getPokemonDetails(id: Int)

}