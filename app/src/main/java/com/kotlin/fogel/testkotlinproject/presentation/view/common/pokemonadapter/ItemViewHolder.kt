package com.kotlin.fogel.testkotlinproject.presentation.view.common.pokemonadapter

import android.view.View
import com.kotlin.fogel.testkotlinproject.custom.recyclerviewadapter.BaseRecyclerViewVH
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import kotlinx.android.synthetic.main.item_pokemon.view.*


class ItemViewHolder(itemView: View) : BaseRecyclerViewVH<Pokemon>(itemView) {

    override fun setListeners(listener: ((Pokemon) -> Unit)?, item: Pokemon?) {
        if (item != null) {
            itemView.setOnClickListener{listener?.invoke(item)}
        }
    }

    override fun bindData(data: Pokemon?) {
        itemView.pokemon_name_text_view.text = data?.name
    }

}