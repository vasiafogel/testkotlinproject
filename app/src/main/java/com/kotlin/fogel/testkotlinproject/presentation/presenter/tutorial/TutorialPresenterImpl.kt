package com.kotlin.fogel.testkotlinproject.presentation.presenter.tutorial

import android.content.SharedPreferences
import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.tutorial.TutorialView


class TutorialPresenterImpl(private val sharedPreferences: SharedPreferences) : BasePresenterImpl<TutorialView>(), TutorialPresenter {

    override fun skip() {
        sharedPreferences
                .edit()
                .putBoolean("getIsViewedTutorial", false)
                .apply()
        view?.openActivity()
    }

    override fun onStart(firstStart: Boolean) {

    }

    override fun onStop() {

    }

    override fun onPresenterDestroyed() {

    }

}