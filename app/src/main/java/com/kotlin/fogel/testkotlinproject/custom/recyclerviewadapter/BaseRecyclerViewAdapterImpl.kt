package com.kotlin.fogel.testkotlinproject.custom.recyclerviewadapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


abstract class BaseRecyclerViewAdapterImpl<T>
    : RecyclerView.Adapter<BaseRecyclerViewVH<T>>(), BaseRecyclerViewAdapter<T> {

    var list: ArrayList<T?> = ArrayList()
    private var listener: ((T) -> Unit)? = null

    override fun onBindViewHolder(holder: BaseRecyclerViewVH<T>?, position: Int) {
        if (listener != null) {
            holder?.setListeners(listener, list[position])
        }
        holder?.bindData(list[position])
    }

    abstract fun getLayout(viewType: Int): Int

    abstract fun getViewHolder(view: View, viewType: Int): BaseRecyclerViewVH<T>?

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseRecyclerViewVH<T>? {
        val inflater = LayoutInflater.from(parent!!.context)
        val view = inflater.inflate(getLayout(viewType), parent, false)
        return getViewHolder(view, viewType)
    }

    override fun getItemCount(): Int = list.size

    override fun addAllAndClear(list: Collection<T?>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun addAll(list: Collection<T?>) {
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun addAllToStart(list: Collection<T?>) {
        this.list.addAll(0, list)
        notifyDataSetChanged()
    }

    override fun add(item: T?) {
        this.list.add(item)
        notifyDataSetChanged()
    }

    override fun addToStart(item: T?) {
        this.list.add(0, item)
        notifyDataSetChanged()
    }

    override fun clear() {
        this.list.clear()
        notifyDataSetChanged()
    }

    override fun remove(position: Int) {
        if (this.list.size > position) {
            this.list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun removeSame() {
        this.list.toSet()
        notifyDataSetChanged()
    }

    override fun removeLast() {
        if (this.list.size > 0) {
            this.list.removeAt(list.size - 1)
            notifyDataSetChanged()
        }
    }

    override fun removeFirst() {
        if (this.list.size > 0) {
            this.list.removeAt(0)
            notifyDataSetChanged()
        }
    }

    override fun getLastItem(): T?  {
        if (list.size != 0) {
            return list.get(list.size-1)
        }else {
            return null
        }
    }

    override fun updateItem(position: Int) {
        if (0 <= position && position < list.size) {
            notifyItemChanged(position)
        }
    }

    override fun changeItem(item: T?, position: Int) {
        if (0 <= position && position < list.size) {
            list[position] = item
            notifyItemChanged(position)
        }
    }

    override fun updateList() = notifyDataSetChanged()

    fun setOnCardClickListener(onCardClickListener: ((T) -> Unit)?) {
        this.listener = onCardClickListener
    }
}
