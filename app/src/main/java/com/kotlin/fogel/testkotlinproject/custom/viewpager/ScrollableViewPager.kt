package com.kotlin.fogel.testkotlinproject.custom.viewpager

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

open class ScrollableViewPager(context: Context, attrs: AttributeSet?) : ViewPager(context, attrs) {

    private var enable: Boolean

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (this.enable) {
            super.onTouchEvent(event)
        } else false

    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (this.enable) {
            super.onInterceptTouchEvent(event)
        } else false

    }

    open fun setPagingEnabled(enabled: Boolean) {
        this.enable = enabled
    }

    init {
        this.enable = true
    }
}