package com.kotlin.fogel.testkotlinproject.presentation.interactor.types.typePage

import com.kotlin.fogel.testkotlinproject.api.service.PokemonService
import com.kotlin.fogel.testkotlinproject.model.Pokemon
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class TypePageInteractorImpl(private val pokemonService: PokemonService) : TypePageInteractor {

    private var disposable: Disposable? = null

    override var networkRequestInProgress = false

    override fun getAllPokemonOfType(id: Int, listener: TypePageInteractor.OnGetAllPokemonOfTypeListener) {
        networkRequestInProgress = true
        disposable = pokemonService.getPokemonType(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    it.pokemon.map {
                        val pokemonId = it.pokemon.url.split("/")[6].toInt()
                        Pokemon(pokemonId, it.pokemon.name)
                    }
                }
                .subscribe({ pokemonList ->
                    networkRequestInProgress = false
                    listener.onSuccess(pokemonList)
                }, { error ->
                    error.printStackTrace()
                    networkRequestInProgress = false
                    listener.onFailure()
                })
    }

    override fun cancelNetworkRequest() {
        disposable?.dispose()
    }

}