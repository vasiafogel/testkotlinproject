package com.kotlin.fogel.testkotlinproject.presentation.presenter.tutorial

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter
import com.kotlin.fogel.testkotlinproject.presentation.view.tutorial.TutorialView


interface TutorialPresenter : BasePresenter<TutorialView> {

    fun skip()

}