package com.kotlin.fogel.testkotlinproject.presentation.presenter.types

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenter
import com.kotlin.fogel.testkotlinproject.presentation.view.types.TypesView


interface TypesPresenter : BasePresenter<TypesView> {

    fun getPokemonTypes()

}