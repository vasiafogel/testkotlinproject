package com.kotlin.fogel.testkotlinproject.presentation.view.main

import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseView


interface MainView : BaseView {

    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun showPokemonList(pokemonList: List<Pokemon?>)
    fun showDoneMessage()
    fun showErrorMessage()

}