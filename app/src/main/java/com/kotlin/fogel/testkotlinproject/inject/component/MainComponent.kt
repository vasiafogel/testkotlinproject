package com.kotlin.fogel.testkotlinproject.inject.component

import com.kotlin.fogel.testkotlinproject.inject.module.MainModule
import com.kotlin.fogel.testkotlinproject.inject.scope.FragmentScope
import com.kotlin.fogel.testkotlinproject.presentation.view.main.MainFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(modules = [(MainModule::class)])
interface MainComponent {

    fun injectTo(fragment: MainFragment)

}