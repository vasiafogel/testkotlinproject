package com.kotlin.fogel.testkotlinproject.model

import android.os.Parcel
import android.os.Parcelable

data class PokemonSprites(
        val frontDefault: String = "",
        val frontShiny: String = "",
        val frontFemale: String = "",
        val frontShinyFemale: String = "",
        val backDefault: String = "",
        val backShiny: String = "",
        val backFemale: String = "",
        val backShinyFemale: String = "") : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(frontDefault)
        parcel.writeString(frontShiny)
        parcel.writeString(frontFemale)
        parcel.writeString(frontShinyFemale)
        parcel.writeString(backDefault)
        parcel.writeString(backShiny)
        parcel.writeString(backFemale)
        parcel.writeString(backShinyFemale)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PokemonSprites> {
        override fun createFromParcel(parcel: Parcel): PokemonSprites {
            return PokemonSprites(parcel)
        }

        override fun newArray(size: Int): Array<PokemonSprites?> {
            return arrayOfNulls(size)
        }
    }
}