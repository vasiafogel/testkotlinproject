package com.kotlin.fogel.testkotlinproject.presentation.view.types.typePage

import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseView


interface TypePageView : BaseView {

    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun getPokemonTypeId() : Int
    fun showAllPokemonOfType(pokemonList: List<Pokemon>)
    fun showDoneMessage()
    fun showErrorMessage()

}