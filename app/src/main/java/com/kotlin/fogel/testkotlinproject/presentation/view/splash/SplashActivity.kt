package com.kotlin.fogel.testkotlinproject.presentation.view.splash

import android.content.SharedPreferences
import android.os.Bundle
import com.kotlin.fogel.testkotlinproject.R
import com.kotlin.fogel.testkotlinproject.extension.app
import com.kotlin.fogel.testkotlinproject.presentation.presenter.loader.PresenterFactory
import com.kotlin.fogel.testkotlinproject.presentation.presenter.splash.SplashPresenter
import com.kotlin.fogel.testkotlinproject.presentation.presenter.splash.SplashPresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseRetainActivity
import com.kotlin.fogel.testkotlinproject.presentation.view.main.MainActivity
import com.kotlin.fogel.testkotlinproject.presentation.view.tutorial.TutorialActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import javax.inject.Inject


class SplashActivity : BaseRetainActivity<SplashPresenter, SplashView>(), SplashView {

    @Inject
    lateinit var sharedPreference: SharedPreferences

    override fun startTutorial() {
        startActivity(intentFor<TutorialActivity>().clearTask())
    }

    override fun openMain() {
        startActivity(intentFor<MainActivity>().clearTask())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun injectDependencies() {
        app.applicationComponent
                .inject(this)
    }

    override fun presenterFactory(): PresenterFactory<SplashPresenter> {
        return object: PresenterFactory<SplashPresenter>{
            override fun create(): SplashPresenter {
                return SplashPresenterImpl(sharedPreference.getBoolean("getIsViewedTutorial", true))
            }
        }
    }
}