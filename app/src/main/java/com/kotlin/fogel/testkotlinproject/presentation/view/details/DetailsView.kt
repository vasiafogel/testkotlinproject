package com.kotlin.fogel.testkotlinproject.presentation.view.details

import com.kotlin.fogel.testkotlinproject.model.Pokemon
import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseView


interface DetailsView : BaseView {

    fun showLoadingIndicator()
    fun hideLoadingIndicator()
    fun getPokemonId(): Int
    fun showPokemonDetails(pokemon: Pokemon)
    fun showDoneMessage()
    fun showErrorMessage()

}