package com.kotlin.fogel.testkotlinproject.presentation.presenter.types

import com.kotlin.fogel.testkotlinproject.presentation.interactor.types.TypesInteractor
import com.kotlin.fogel.testkotlinproject.model.Type
import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.types.TypesView


class TypesPresenterImpl(private val interactor: TypesInteractor) :
        BasePresenterImpl<TypesView>(), TypesPresenter, TypesInteractor.OnGetPokemonTypesListener {

    private var pokemonTypes = listOf<Type>()

    override fun onStart(firstStart: Boolean) {
        if (firstStart) {
            getPokemonTypes()
        } else {
            if (interactor.networkRequestInProgress) {
                view?.showLoadingIndicator()
            }
            view?.showPokemonTypes(pokemonTypes)
        }
    }

    override fun onStop() {
        view?.hideLoadingIndicator()
    }

    override fun onPresenterDestroyed() {
        interactor.cancelNetworkRequest()
    }

    // TypesPresenter

    override fun getPokemonTypes() {
        view?.showLoadingIndicator()
        interactor.getPokemonTypes(this)
    }

    // OnGetPokemonTypesListener

    override fun onSuccess(pokemonTypes: List<Type>) {
        this.pokemonTypes = pokemonTypes

        view?.showPokemonTypes(pokemonTypes)
        view?.hideLoadingIndicator()
        view?.showDoneMessage()
    }

    override fun onFailure() {
        view?.hideLoadingIndicator()
        view?.showErrorMessage()
    }
}