package com.kotlin.fogel.testkotlinproject.presentation.interactor.types

import com.kotlin.fogel.testkotlinproject.presentation.interactor.base.BaseInteractor
import com.kotlin.fogel.testkotlinproject.model.Type


interface TypesInteractor : BaseInteractor {

    interface OnGetPokemonTypesListener {
        fun onSuccess(pokemonTypes: List<Type>)
        fun onFailure()
    }

    var networkRequestInProgress: Boolean

    fun getPokemonTypes(listener: OnGetPokemonTypesListener)
    fun cancelNetworkRequest()

}