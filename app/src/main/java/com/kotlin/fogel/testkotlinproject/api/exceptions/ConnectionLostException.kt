package com.kotlin.fogel.testkotlinproject.api.exceptions


class ConnectionLostException(msg: String = "Internet connection lost") : RuntimeException(msg)