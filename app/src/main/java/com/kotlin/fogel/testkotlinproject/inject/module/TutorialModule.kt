package com.kotlin.fogel.testkotlinproject.inject.module

import com.kotlin.fogel.testkotlinproject.custom.viewpager.adapter.BaseViewPagerAdapter
import com.kotlin.fogel.testkotlinproject.inject.scope.ActivityScope
import dagger.Module
import dagger.Provides


@Module
class TutorialModule {

    @Provides
    @ActivityScope
    fun provideViewPagerAdapter(): BaseViewPagerAdapter {
        return BaseViewPagerAdapter()
    }

}
