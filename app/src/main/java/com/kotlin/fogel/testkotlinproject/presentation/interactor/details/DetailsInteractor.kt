package com.kotlin.fogel.testkotlinproject.presentation.interactor.details

import com.kotlin.fogel.testkotlinproject.presentation.interactor.base.BaseInteractor
import com.kotlin.fogel.testkotlinproject.model.Pokemon


interface DetailsInteractor : BaseInteractor {

    interface OnGetPokemonDetailsListener {
        fun onSuccess(pokemon: Pokemon)
        fun onFailure()
    }

    var networkRequestInProgress: Boolean

    fun getPokemonDetails(id: Int, listener: OnGetPokemonDetailsListener)
    fun cancelNetworkRequest()

}