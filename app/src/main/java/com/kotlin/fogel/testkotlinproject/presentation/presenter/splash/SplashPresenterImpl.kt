package com.kotlin.fogel.testkotlinproject.presentation.presenter.splash

import com.kotlin.fogel.testkotlinproject.presentation.presenter.base.BasePresenterImpl
import com.kotlin.fogel.testkotlinproject.presentation.view.splash.SplashView
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit


class SplashPresenterImpl(private var isViewedTutorial: Boolean)
    : BasePresenterImpl<SplashView>(), SplashPresenter  {

    private var disposable: Disposable? = null


    override fun onStart(firstStart: Boolean) {
        disposable = Observable.just(isViewedTutorial)
                .delay(3, TimeUnit.SECONDS)
                .subscribe {
                    when {
                        it -> view?.startTutorial()
                        else -> view?.openMain()
                    }
                }
    }

    override fun onStop() {

    }

    override fun onPresenterDestroyed() {
        disposable?.dispose()
    }
}