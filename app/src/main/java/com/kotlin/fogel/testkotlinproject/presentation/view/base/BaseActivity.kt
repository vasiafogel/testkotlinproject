package com.kotlin.fogel.testkotlinproject.presentation.view.base

import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import com.kotlin.fogel.testkotlinproject.R
import org.jetbrains.anko.activityManager
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.longToast
import org.jetbrains.anko.find


abstract class BaseActivity : AppCompatActivity() {

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

    fun hideKeyboard(){
        if (currentFocus != null)
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.about -> {
                about()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun about() {
        Snackbar.make(find<CoordinatorLayout>(R.id.coordinator_layout), R.string.about_message, Snackbar.LENGTH_SHORT)
                .show()
    }

}