package com.kotlin.fogel.testkotlinproject.presentation.view.splash

import com.kotlin.fogel.testkotlinproject.presentation.view.base.BaseView


interface SplashView : BaseView {

    fun startTutorial()
    fun openMain()

}